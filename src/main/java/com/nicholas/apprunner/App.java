package com.nicholas.apprunner;

import com.nicholas.display.MatrixDisplay;
import com.nicholas.operations.Multiplication;
import com.nicholas.operations.SumAndDif;

public class App {

    public static void main(String[] args) {

        int[][] matrix01 = {
                {1, 2, 3, 4, 5},
                {6, 7, 8, 9, 10},
                {11, 12, 13, 14, 15},
                {16, 17, 18, 19, 20},
                {21, 22, 23, 24, 25}
        };
        int[][] matrix02 = {
                {31, 32, 33, 34, 35},
                {36, 37, 38, 39, 40},
                {41, 42, 43, 44, 45},
                {46, 47, 48, 49, 50},
                {51, 52, 53, 54, 55}
        };

//        SumAndDif obj2 = new SumAndDif();
        MatrixDisplay obj = new MatrixDisplay();
//        obj.showMatrix(obj2.matrixSum(matrix01, matrix02));
//        System.out.println("========================");
//        obj.showMatrix(obj2.matrixDif(matrix01, matrix02));

        Multiplication mtl = new Multiplication();
        obj.showMatrix(mtl.multiplication(matrix01, matrix02));



    }
}
