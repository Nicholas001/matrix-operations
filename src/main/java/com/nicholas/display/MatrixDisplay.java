package com.nicholas.display;

public class MatrixDisplay {

    public void showMatrix(int[][] matrix) {

        for (int x = 0; x < matrix.length; x++) {
            for (int y = 0; y < matrix[x].length; y++) {
                System.out.print(" " + matrix[x][y]);
            }
            System.out.println(" ");
        }

    }

}
