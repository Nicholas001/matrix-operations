package com.nicholas.operations;

public class Multiplication {

    public int[][] multiplication(int[][] matrix1, int[][] matrix2) {
        int[][] result = new int[matrix1.length][matrix1[0].length];

        for (int x = 0; x < matrix1.length; x++) {
            for (int y = 0; y < matrix1.length; y++) {
                for (int z = 0; z < matrix1.length; z++) {
                    result[x][y] = result[x][y] + matrix1[x][z] * matrix2[z][y];
                }
            }
        }
        return result;
    }
}
