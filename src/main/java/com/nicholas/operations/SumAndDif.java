package com.nicholas.operations;

public class SumAndDif {

    public int[][] matrixSum(int[][] matrix1, int[][] matrix2) {

//        matricea data ca si parametru trebuie sa fie patratica

        int[][] result = new int[matrix1.length][matrix1.length];

        for (int x = 0; x < matrix1.length; x++) {
            for (int y = 0; y < matrix1[x].length; y++) {
                result[x][y] = matrix1[x][y] + matrix2[x][y];
            }
        }


        return result;
    }

    public int[][] matrixDif(int[][] matrix1, int[][] matrix2) {

//        matricea data ca si parametru trebuie sa fie patratica

        int[][] result = new int[matrix1.length][matrix1.length];

        for (int x = 0; x < matrix1.length; x++) {
            for (int y = 0; y < matrix1[x].length; y++) {
                result[x][y] = matrix1[x][y] - matrix2[x][y];
            }
        }

        return result;
    }

}
