package com.nicholas.display;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestDisplay {

    @BeforeClass
    public static void beforeClass() {
        System.out.println("BeforeClass was called!");
    }

    @Before
    public void setUp() {
        System.out.println("SetUp was called!");
    }

    @Test
    public void testDisplay() {
        int[][] matrix01 = {
                {1, 2, 3, 4, 5},
                {6, 7, 8, 9, 10},
                {11, 12, 13, 14, 15},
                {16, 17, 18, 19, 20},
                {21, 22, 23, 24, 25}
        };

        MatrixDisplay obj = new MatrixDisplay();
        obj.showMatrix(matrix01);
    }

    @After
    public void tearDown() {
        System.out.println("TearDown was called!");
    }

}
