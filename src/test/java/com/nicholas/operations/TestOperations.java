package com.nicholas.operations;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestOperations {

        @BeforeClass
        public static void beforeClass() {
            System.out.println("BeforeClass was called!");
        }

        @Before
        public void setUp() {
            System.out.println("SetUp was called!");
        }

        @Test
        public void testMatrixSum() {
            int[][] matrix01 = {
                    {1, 2, 3, 4, 5},
                    {6, 7, 8, 9, 10},
                    {11, 12, 13, 14, 15},
                    {16, 17, 18, 19, 20},
                    {21, 22, 23, 24, 25}
            };
            int[][] matrix02 = {
                    {31, 32, 33, 34, 35},
                    {36, 37, 38, 39, 40},
                    {41, 42, 43, 44, 45},
                    {46, 47, 48, 49, 50},
                    {51, 52, 53, 54, 55}
            };
            SumAndDif obj = new SumAndDif();
            obj.matrixSum(matrix01,matrix02);

        }

        @Test
        public void testMatrixDif() {
            int[][] matrix01 = {
                    {1, 2, 3, 4, 5},
                    {6, 7, 8, 9, 10},
                    {11, 12, 13, 14, 15},
                    {16, 17, 18, 19, 20},
                    {21, 22, 23, 24, 25}
            };
            int[][] matrix02 = {
                    {31, 32, 33, 34, 35},
                    {36, 37, 38, 39, 40},
                    {41, 42, 43, 44, 45},
                    {46, 47, 48, 49, 50},
                    {51, 52, 53, 54, 55}
            };
            SumAndDif obj = new SumAndDif();
            obj.matrixDif(matrix01,matrix02);

        }

        @After
        public void tearDown() {
            System.out.println("TearDown was called!");
        }

}
