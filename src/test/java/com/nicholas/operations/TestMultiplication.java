package com.nicholas.operations;

import org.junit.*;

public class TestMultiplication {

    @BeforeClass
    public static void beforeClass() {
        System.out.println("BeforeClass was called!");
    }

    @Before
    public void setUp() {
        System.out.println("SetUp was called!");
    }

    @Test
    public void testMatrixMultiplications() {
        int[][] matrix01 = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };
        int[][] matrix02 = {
                {10, 11, 12},
                {13, 14, 15},
                {16, 17, 18}
        };
        Multiplication obj = new Multiplication();
        int[][] test;

        test = obj.multiplication(matrix01, matrix02);
        Assert.assertEquals((1 * 10 + 2 * 13 + 3 * 16), test[0][0]);
        Assert.assertEquals((7 * 12 + 8 * 15 + 9 * 18), test[matrix01.length-1][matrix01.length-1]);

    }

    @After
    public void tearDown() {
        System.out.println("TearDown was called!");
    }


}
